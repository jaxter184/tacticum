use clap::Clap;
use std::io;
use std::collections::HashMap;
use std::collections::VecDeque;
use tacticum::serial as tact;
use tacticum::game;
use tacticum::piece;
use tacticum::ability;
//#[path = "piecepool.rs"] mod piecepool;
/// Turn-based deck building strategy game that uses pieces rather than cards
#[derive(Clap)]
#[clap(version = "0.1.0", author = "jaxter184")]
struct Opts {
	/// Set a custom config file
	#[clap(short = "c", long = "config", default_value = "~/.config/tacticum/config")]
	config: String,
	#[clap(subcommand)]
	subcmd: SubCommand,
}

#[derive(Clap)]
enum SubCommand {
	/// Build a kit
	Build(Build),
	/// Continue playing a game game
	Play(Play),
	/// Start a new game
	New(New),
}

#[derive(Clap)]
struct Build {
	/// Indicate level of verbosity; can be used multiple times
	#[clap(short = "v", long = "verbose", parse(from_occurrences))]
	verbose: i32,
	/// Kit path
	kit: String,
	#[clap(short = "p", long = "pool")]
	pool: Vec<String>,
	#[clap(subcommand)]
	subcmd: BuildSubCommand,
}

#[derive(Clap)]
enum BuildSubCommand {
	/// List all pieces in the current kit
	List(List),
	/// Add a piece to the current kit
	Add(Add),
}

#[derive(Clap)]
struct List {
}

#[derive(Clap)]
struct Add {
	/// Path of the piece to add to the kit
	piece_path: String,
}

#[derive(Clap)]
struct New {
//	/// Indicate level of verbosity; can be used multiple times
//	#[clap(short = "v", long = "verbose", parse(from_occurrences))]
//	verbose: i32,
	/// Paths to player kits
	players: Vec<String>,
//	/// Difficulty (0-5)
//	#[clap(short = "d", long = "difficulty", default_value = "2")]
//	difficulty: i8,
}

#[derive(Clap)]
struct Play {
//	/// Indicate level of verbosity; can be used multiple times
//	#[clap(short = "v", long = "verbose", parse(from_occurrences))]
//	verbose: i32,
	/// Path to game
	game: String,
}

// adapted from code by Boiethios: https://stackoverflow.com/questions/50277050/is-there-a-built-in-function-that-converts-a-number-to-a-string-in-any-base
fn i_to_r36(mut x: u32) -> String {
	let mut result = vec![];
	loop {
		let m = x % 36;
		x = x / 36;

		result.push(std::char::from_digit(m, 36).unwrap());
		if x == 0 {
			break;
		}
	}
	result.into_iter().rev().collect()
}

fn main() {
	let opts: Opts = Opts::parse();

	// Gets a value for config if supplied by user, or defaults to "default.conf"
	println!("Value for config: {}", opts.config);

	match opts.subcmd {
		SubCommand::Build(t) => {
			if t.verbose > 1 {
				println!("verbose info: {}", t.verbose);
			}
			let mut pools = Vec::<tact::Kit>::new();
			// Load/create kit for editing
			let mut kit = if std::path::Path::new(&t.kit).exists() {
				println!("Editing kit: {}", &t.kit);
				tact::Kit::load(&t.kit).unwrap()
			} else {
				println!("Creating kit: {}", &t.kit);
				tact::Kit::new()
			};
			// Load additional kits to pull pieces from
			for ea_pool in t.pool {
				if std::path::Path::new(&ea_pool).exists() {
					pools.push(tact::Kit::load(&ea_pool).expect("Pool could not be loaded"));
				}
				else {
					println!("Pool not found: {}", ea_pool);
				}
			}
			match t.subcmd {
				BuildSubCommand::List(_build_t) => {
					println!("{}", kit);
				}
				BuildSubCommand::Add(build_t) => {
					if !kit.contains_piece(&build_t.piece_path) {
						// If piece is not yet in the edited kit
						let mut missing = true;
						for ea_pool in pools {
							match ea_pool.get_piece(&build_t.piece_path) {
								Some(piece) => {
									kit.insert_piece(&build_t.piece_path, piece.clone());
									missing = false;
									break;
								}
								None => (),
							}
						}
						if missing {
							println!("Piece could not be found in any of the piecepools")
						}
					}
					kit.push_path(&build_t.piece_path);
					kit.save(&t.kit).unwrap();
				}
			}
		}
		SubCommand::New(t) => {
			let rcrc_g = game::Game::new();
			{
				let mut game = rcrc_g.borrow_mut();
				game.set_board(game::Board::new_annulus_board(2,3));
				for ea_player in t.players {
					if std::path::Path::new(&ea_player).exists() {
						let kit = tact::Kit::load(&ea_player).expect("Kit could not be loaded");
						let player = game::Player::new();
						player.borrow_mut()
							.set_name(kit.get_title().clone()) // Get player name from kit name
							.set_source(game::DecisionSource::Client) // Make player playable (as opposed to CPU)
							.set_pieces(kit.to_pieces()); // Copy pieces from kit into player
						game.push_player(player);
					}
					else {
						println!("Player's kit not found: {}", ea_player);
					}
				}
				print_board(&game, Pb::Piece, None);
//				println!("{}", rcrc_g.borrow().pieces_to_string());
				let mut deciding = true;
				while deciding {
					print_time(&game);
					// A decision is a sequence of choices, namely an action followed by a series of targets
					let mut decision: (Option<game::Action>, VecDeque<ability::Target>) = (None, VecDeque::new());
					// Get the full list of possible options for the current player
					game.generate_choice_tree();
					let choice_tree = game.get_choice_tree();
					loop {
						let mut choices = HashMap::new();
						let mut target_idx = 0;
						println!("{:?}", decision);
						for ea_choice in choice_tree.get_next_options(&decision).expect("Invalid error") {
							match &ea_choice {
								game::Choice::Target(t) => match t {
									ability::Target::Position(_) => {
										choices.insert(i_to_r36(target_idx), ea_choice);
										target_idx += 1;
									},
									_ => {choices.insert("poop".to_string(), ea_choice);}
								},
								game::Choice::Action(a) => {
									choices.insert(action_to_string_token(&a), ea_choice);
								}
							}
						}
						match choices.len() {
							0 => break, // resolve choice
							1 => (), // TODO: autopick
							_ => (),
						};
						if target_idx != 0 {
							print_board(&game, Pb::Piece, Some(&choices));
						}
						println!("=== Playing as: {}", game.get_current_priority_player().borrow().get_name());
						for (ea_name, ea_choice) in &choices {
							println!("{} - {}", &ea_name.clone(), choice_to_string_description(ea_choice));
						}
						println!("board - show board");
						println!("quit - save game and quit");

						let mut input = String::new();
						io::stdin().read_line(&mut input).expect("text could not be read");
						match input.trim() {
							"quit" => {
								println!("Leaving game");
								deciding = false;
								break;
							},
							"board" => print_board(&game, Pb::Piece, None),
						//"test" => {println!("{}", game.expand_range(game::Position::new(3,0), game::Range::new((0, 0), 1)).len());},
							"coord" => print_board(&game, Pb::Coord, None),
							"dir" => print_board(&game, Pb::Direction, None),
							_ => match choices.get(input.trim()) {
								Some(ch) => {
									match ch {
										game::Choice::Action(a) => {
											decision.0 = Some(a.clone());
										}
										game::Choice::Target(t) => {
											decision.1.push_back(t.clone());
										}
									}
								}
								None => println!("Option not found"),
							},
						}
					}
					if !deciding { // TODO: make this neater
						break;
					}
					game.resolve(decision.0.unwrap(), decision.1);
				}
			}
		},
		SubCommand::Play(t) => {
			println!("play {}", t.game);
		}
	}
}

enum Pb {
	Piece,
	Coord,
	Direction,
}

fn print_board(game: &game::Game, mode: Pb, choices: Option<&HashMap<String, game::Choice>>) {
	let mut printable_board: Vec<Vec<String>> = Vec::new();
	let dims = game.get_board().get_bounds();
	for _y in 0..((dims.1).1) {
		let mut row = Vec::new();
		for _x in 0..((dims.1).0) {
			row.push(" ".to_string());
		}
		printable_board.push(row);
	}
	let mut piece_map = HashMap::new();
	for (ea_pos, ea_cell) in game.get_board().get() {
		match mode {
			Pb::Direction => { // Show directional flow
				let ea_cell_dir = ea_cell.get_directions();
				printable_board[(ea_pos.get_y() - (dims.0).1) as usize][(ea_pos.get_x() - (dims.0).0) as usize] = if ea_cell_dir.len() != 1 { '*' }
				else {
					match ea_cell_dir.iter().next().unwrap() {
						game::Direction::NE => 'n',
						game::Direction::E => 'e',
						game::Direction::SE => 'x',
						game::Direction::SW => 's',
						game::Direction::W => 'w',
						game::Direction::NW => 'z',
					}
				}.to_string();
			},
			Pb::Coord => { // Show coordinates at each position
				printable_board[(ea_pos.get_y() - (dims.0).1) as usize][(ea_pos.get_x() - (dims.0).0) as usize] = format!("{},{}", ea_pos.get_x(), ea_pos.get_y());
			},
			Pb::Piece => { // Show piece positions
				printable_board[(ea_pos.get_y() - (dims.0).1) as usize][(ea_pos.get_x() - (dims.0).0) as usize] = ".".to_string();
			},
		}
	}
	match mode {
		Pb::Piece => {
			for ea_piece in &game.get_pieces() {
				match ea_piece.borrow().get_placement() {
					piece::Placement::Position(p) => {
						piece_map.insert(p, ea_piece.borrow().get_name().chars().next().unwrap_or_else(|| '*').to_string());
					},
					_ => (),
				}
			}
			for (ea_piece_pos, ea_piece_char) in piece_map {
				printable_board[(ea_piece_pos.get_y() - (dims.0).1) as usize][(ea_piece_pos.get_x() - (dims.0).0) as usize] = ea_piece_char;
			}
		}, _ => (),
	}
	match choices {
		Some(c) => for (ea_token, ea_choice) in c {
			match ea_choice {
				game::Choice::Target(t) => match t {
					ability::Target::Position(pos) => {
						printable_board[(pos.get_y() - (dims.0).1) as usize][(pos.get_x() - (dims.0).0) as usize] = ea_token.clone();
					},
					_ => (),
				}
				_ => (),
			}
		},
		None => (),
	}
	let mut spaces = 0;//printable_board.len();
	for ea_row in printable_board {
		for _spaces in 0..spaces {
			print!("  ");
		}
		spaces += 1;
		for ea_col in ea_row {
			print!("{}", ea_col);
			print!("   ");
		}
		print!("\n");
	}
}

fn print_time(game: &game::Game) {
	println!("Turn: {}", game.get_turn().to_string());
	println!("{}'s turn", game.get_current_turn_player().borrow().get_name());
	println!("Phase: {}", game.get_phase().to_string());
	println!("Acting: {}", game.get_current_priority_player().borrow().get_name());
}

fn choice_to_string_description(choice: &game::Choice) -> String {
	match &choice {
		game::Choice::Action(a) => match a {
			game::Action::End => "End the phase".to_string(),
			game::Action::Pass => "Pass priority to the other player".to_string(),
			game::Action::PlacePiece(i) => format!("Place {}", i.borrow_mut().get_name().to_string()),
	//		UseAbility(i),
	//		TargetPiece(i),
	//		TargetPosition(i),
	//		TargetQueuePosition(i),
			_ => unimplemented!(),
		},
		game::Choice::Target(t) => match t {
			ability::Target::Position(pos) => format!("Target position {},{}", pos.get_x(), pos.get_y()).to_string(),
			_ => unimplemented!(),
		}
	}
}

fn action_to_string_token(action: &game::Action) -> String {
	match action {
		game::Action::End => "end".to_string(),
		game::Action::Pass => "pass".to_string(),
		game::Action::PlacePiece(i) => i.borrow_mut().get_path().to_string(),
//		game::Action::UseAbility(i),
//		game::Action::TargetPiece(i),
//		game::Action::TargetPosition(i) => format!("{},{}", i.get_x(), i.get_y()).to_string(),
//		game::Action::TargetQueuePosition(i),
		_ => unimplemented!(),
	}
}

