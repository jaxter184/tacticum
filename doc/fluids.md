### fluids and their canonical colors

* blood: red
* honey (glucose): yellow
* jelly (acid): green
* ethyl (alcohol): cyan
* lipid (grease): blue
* virus (poison): magenta
* water: gray

### individual qualities

* b: strength/
* h: unity/fragmentation
* j: security/exploitation
* e: utility/inhibition
* l: ergonomics/obstruction
* v: proliferation/decay
* w: equivalence/imbalance

maybe consider:
* rigidity/integrity
* organization/formation
* efficiency
* purpose/perfection
* flexibility/adaptability
* modularity/change
* underdogs/modesty
* uniformity/simplicity
* self-sufficiency
* combination/permutation
* freedom/ability

* b: survival of the fittest, trial and error
* h: united we stand, divided we fall
* j: the bigger they are, the harder they fall
* e: practice makes perfect
* l: work smarter, not harder
* v: nothing ventured, nothing gained
* w: 

maybe consider:
* don't count your chickens before they hatch

### subdivisions/group qualities

* boost: bhj
* debuff: egv

* ?: vbh
* ?: jeg

* ?: gvb
* ?: hje

* self: bjg
* else: hev

* health: hj
* speed: je
* damage: vb
* spread: vg

* hj: excess
* bv: lineage
* el: technology

### just for fun

water,	doc,	sloth,	aether,	sun,	antarctica,	mail-upgrade it
blood,	dopey,	wrath,	fire,	mon,	america,	use it
honey,	happy,	glttny,	earth,	tue,	africa ,	buy it
jelly,	sleepy,	greed,	wood,	wed,	australia,	change it
ethyl,	grumpy,	envy,	water,	thu,	europe ,	fix it
lipid,	bashfl,	pride,	metal,	fri,	sealand,	trash it
virus,	sneezy,	lust,	air,	sat,	asia(hmmm),	break it

