* pieces cannot occupy the same space
* movement range is effectively attack range
* some pieces cannot move?
* some pieces can increase or decrease the size of the board
* ability that can change the size of the board with a 1 turn delay?
* having all pieces available for deck building evens the playing field, but drafts are still a thing
* what should the restrictions of fluids/colors be? tribals?
* environment pieces?
* neutral (teamless) pieces?
* game has to be shorter than chess in terms of turns because it is more complicated, and turns take more time
* starting end of turn 6(ish), players can spend 1 action point to increase maximum action points by 1.
* hidden information makes things interesting, but makes the game harder to build. in addition, cheating becomes more possible. by removing hidden information, cheating becomes trivially easy to reveal. even with this benefit, i want to be able to put the spiciness of hidden information back into the game without actually putting hidden information into the game. how do you bluff when both players have perfect information?
* in chess, there is hidden information: your opponent's strategy. find a way to maximize the usefulness of that hidden information.
* territory mechanic? start out with some territory, and it grows as you move your pieces around
	* some pieces can only target pieces on allied territory
	* territory difference does stuff too
