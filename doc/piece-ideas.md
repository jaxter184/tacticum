# mechanics

health (resource): resets at start of owner's turn
shield (resource): enemy abilities that target this piece give it +1h
charge (resource): spent when an ability is used. can be on either a piece or an ability (or a player, like move charges). replenished at the beginning of the owner's start phase. movement uses charges, but usually starts at 0.
attack (resource): spent when a piece attacks. replenished at the beginning of each start phase.
surplus: additional resource that does not replenish
max health (ability): at the beginning of the players start phase, set this to 1
mortality (ability): if health resource is 0 or less, destroy self
immortality (ability): if health resource is 0 or less, this piece gets -1 life. then if life resource is 0 or less, destroy self
petrify (ability): target piece's move counters are set to 0
push/pull (ability): moves a target piece away/towards source piece
merge (ability): destroy target allied piece. destroy self and replace with a different piece.
cloaked (ability): self cannot be targeted. if it uses any abilities, cloaking is released
unique (ability): there can no other pieces of the same type as self on the board
disguised (ability): self appears to be a different piece to the enemy

# pieces

### key
* h: health
* s: shield
* c: charge
* l: lives
* m: meters (up to *)
* ^: maximum
* <: less than or equal to
* +: additional
* a: attack (deal * damage to target piece)

## divine

helios (12, jelly): 4l, 1h^, 1m
	owner gets ^3m
	1a 1m
	target cloaked piece is revealed (1c)
	at start of game, all disguised pieces are revealed
vector (11, grease): 5l, 1h^ 1m forward
	owner gets ^2m
	1a 1m
	other target piece can move <2m in any direction until end of phase (2c)
	passive: all allied pieces can move in any direction that vector can
rotpot (12, grease): 5l, 2h^, 1m
	owner gets ^2m
	target allied piece faces a different direction 4c^
	3a 1mf
jetset (15, lipid): 3l, 1h^, 1m (3c^)
	owner gets ^3m
	1a 1m
	can move during attack phase
	+1mc to target piece 2m (1c^)
	+1mc to all pieces 1m (1c^)
yang (15, honey): 3l, 1h^ 1m
	owner gets ^3m
	idk something ubi i guess
plaque (14, virus): 1l, 1h^, 1m
	owner gets ^2m
	destroy all pieces 1m except plaque
chisaki (13, virus, ethyl): 1l, 1h^, 1m
	owner gets ^2m
	destroy self, then add all of this piece's abilities to piece 1m
meat (9, blood): 5l, 1h^, 1m
	owner gets ^2m
	at start of owner's turn, all pieces get an ability:
		1a 1m (1c)
mistew obama (9, lipid) 5l, 1h^, 1m
	owner gets ^2m
	[target ability], then [target ability]'s owner gains ability:
		either:
			activate [target ability], then destroy this ability
			if it is the start of the turn, destroy self
mothership (12, ethyl) 3l, 4h^ never resets, 1m
	all allied pieces 1m except self are cloaked until end of phase
	1a to all 1m
megachessatron (15, honey) 1l, 8h^
	5a flr
	cannot move

## ranked

shield maiden (6, jelly): 4h^ 2m
	all allied pieces 1m get +1s until the end of the turn (1c^)
	1a 1m f, set target's move charge to 0 (seems like a lipid thing to do)
swarm host (6, virus): 3h^, 1m
	3 cell footprint
	create a larva on target cell <1m. (1c^, only on owner's move phase)
larva (0, virus): 1h^, 3m flr
	starts with 3 life charges
	at end of turn, decrease number of life charges by 1
	if life charges == 0, destroy self
	1a 1m f
substitute (5, ethyl): 5h^, 1m
	at start of owner's turn, +1 swap charge
	use 2 swap charges to swap places with target piece 1m
	use 4 swap charges to swap places with target piece (1c)
archon (10, ethyl): 2h^, 2s, 3m
	2a to all 1m flr
immolant (7, blood, ethyl): 3h, 1m, 3m flr
	all pieces 1m flr get ability: at end of turn, -1h
	1a 1m f
	whenever an enemy piece is destroyed during the end phase, +1h
objection (9, ethyl):
	if [target ability] fails before the end of the turn, remove [target ability].
	2a 1m f
project e (9, ethyl): 1h^, 1m
	copy all of target piece's abilities to self
	at start of each turn, remove one ability from self
project h (9, honey): 3h^, 3m f
	(only during move) either: (1c^)
		jump self to target cell 1m from an allied piece
		jump target piece to cell 1m from self
project j (9, jelly): 3h^, 3m fds
	jump to cell behind target enemy piece 2m fds (pshh... nothing personnel, kid)
	4a 1m f
project b (9, blood): 2h^, 1s, 3m flr
	cloaked
	at start of turn, +1c drill
	spend all drill charge to do that much damage to target enemy piece 1m frdsl
project l (9, lipid): 
	target enemy piece 2m diagonal cannot move until start of self's owner's next turn

## pawn

railgun (4, grease): 3h^, 1m
	push target piece <1m to cell <3m, then deal 3 damage to all pieces 1m from target piece
templar (5, ethyl): 1h^, 1s, 2m jump even
	merge with a templar within 1m to create an archon
	at the beginning of the end step, 1a to all 2m (star)
taiko (5, blood): 3h^, 1m
	target creature 2m gets +1ac (1c^)
heal bunny (6, honey): 3h^, 2m
	at start of owner's turn, +2c heal
	use 1c heal, then restore health on target piece 2m
phlanax (6, honey, blood): 1h^, 1m
	at start of owner's turn, allied pieces 1m rbl get +1s, +1ac
	1a 1m frl
pawn (3, blood): 4h^, 2m f
	4a 1m frl
pathogen (3, virus): 1h^, 2s, 1m fds
	1a 1m rbl (to allies as well)
ball nut (3 ethyl): 2h^, 1s, 2m (2c^)
	cloaked
	2a 1m flr
ranger (3, honey): 3h^, 2m flr
	3a exactly 2m jump f*(flr) or 1a 1m flr
self-repair robot (3, grease): 3l, 1h^, 2m
	health is replenished at end of owner's turn instead of start
	at end of turn, if <0h, -1 gear, then set 1m
automaton (3, grease): 2h^, 2m
	movement doesn't require player resource
	at end of turn, 1a all 1m
hero (4, honey, blood): 2h^, 2m
	starts with 1 level (resource)
	deal damage equal to the level resource to target enemy 1m, then if the piece's health is 0 or less, add 1 level (resource)
cherub (4, honey): 2h^, 2m
	if chorus charge >= 3:
		3a all 1m flr
	else:
		1a 1m flr
		then +1 chorus charge to all allies 2m

