use serde_derive::{Serialize, Deserialize};
use std::collections::HashMap;
use std::collections::HashSet;
use std::rc::Rc;
use std::cell::RefCell;
use std::hash::{Hash, Hasher};
use super::{*, ability::Resource};

#[cfg(test)]
pub mod piece_tests {
	use super::*;
	pub fn make_pawn() -> Rc<RefCell<Box<Piece>>> {
		let mut movement = ability::Ability::new();
		movement.borrow_mut().set_name("Move".to_string())
		.set_activation_condition(Some(ability::Condition::Game(ability::GameCondition::IsPhase(game::Phase::Move))))
		.set_effect(ability::Effect::MoveSelf( ability::TargetPositionCondition::And(vec![
			ability::TargetPositionCondition::IsUnoccupied,
			ability::TargetPositionCondition::IsInRange{direction: game::Position::new(0,0), range: Some(1), line_of_sight: true}
		])))
		.insert_resource("Move".to_string(), Resource::new().set_amount(0).set_default(3).set_reset(None).clone());
		let p = Rc::new(RefCell::new(Box::new(Piece::new())));
			p.borrow_mut().set_name("pawn".to_string())
			.set_path("/test/pawn".to_string())
			.set_rank(2)
			.push_ability(movement)
			.set_placement(Placement::Unplayed);
		let mut health = Resource::new();
		health.set_amount(3).set_default(3).set_reset(None);
		let mut mov = Resource::new();
		mov.set_amount(0).set_default(3).set_reset(None);
		p.borrow_mut().insert_resource("Health".to_string(), health);
		p.borrow_mut().insert_resource("Move".to_string(), mov);
//		println!("{}", kit.to_string().unwrap());
//		kit.to_serial().save_standalone(&"test.json".to_string()).expect("Err: save failed");
		p
	}
	pub fn make_main() -> Rc<RefCell<Box<Piece>>> {
		let mut movement = ability::Ability::new();
		movement.borrow_mut().set_name("Move".to_string())
		.set_activation_condition(Some(ability::Condition::Game(ability::GameCondition::IsPhase(game::Phase::Move))))
		.set_effect(ability::Effect::MoveSelf(ability::TargetPositionCondition::IsUnoccupied))
		.insert_resource("Move".to_string(), Resource::new().set_amount(0).set_default(3).set_reset(None).clone());
		let p = Rc::new(RefCell::new(Box::new(Piece::new())));
			p.borrow_mut().set_name("lloyd".to_string())
			.set_path("/test/main".to_string())
			.push_ability(movement.clone())
			.set_placement(Placement::Unplayed);
		movement.borrow_mut().set_owner(&p);
		let mut health = Resource::new();
		health.set_amount(3).set_default(3).set_reset(None);
		let mut mov = Resource::new();
		mov.set_amount(0).set_default(3).set_reset(None);
		p.borrow_mut()
		.insert_resource("Health".to_string(), health)
		.insert_resource("Move".to_string(), mov);
//		println!("{}", kit.to_string().unwrap());
//		kit.to_serial().save_standalone(&"test.json".to_string()).expect("Err: save failed");
		p
	}
}

#[derive(Clone, PartialEq, Debug)]
pub struct Piece {
	owner: Option<Rc<RefCell<Box<game::Player>>>>,
	name: String,
	path: String,
	rank: u8,
	attributes: HashSet<String>,
	abilities: Vec<Rc<RefCell<Box<ability::Ability>>>>,
	placement: Placement,
	resources: HashMap<String, Resource>,
}
impl std::fmt::Display for Piece {
	fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
		write!(f, "Unit: {}", self.name)
	}
}
impl Hash for Piece {
	fn hash<H: Hasher>(&self, state: &mut H) {
		0.hash(state);
	}
}
impl Piece {
	pub fn new() -> Self { Piece {owner: None, name: "".to_string(), path: "".to_string(), rank: 0, attributes: HashSet::new(), abilities: vec![], placement: Placement::Unplayed, resources: HashMap::new()} }
	pub fn set_owner(&mut self, owner: Rc<RefCell<Box<game::Player>>>) -> &mut Self { self.owner = Some(owner); self }
	pub fn get_owner(&self) ->        &Rc<RefCell<Box<game::Player>>> { &self.owner.as_ref().unwrap() }
	pub fn set_name(&mut self, name: String) -> &mut Self { self.name = name; self }
	pub fn get_name(&self) ->   &String { &self.name }
	pub fn set_path(&mut self, path: String) -> &mut Self { self.path = path; self }
	pub fn get_path(&self) ->   &String { &self.path }
	pub fn set_rank(&mut self, rank: u8) -> &mut Self { self.rank = rank; self }
	pub fn get_rank(&self) ->   &u8 { &self.rank }
	pub fn set_placement(&mut self, placement: Placement) -> &mut Self { self.placement = placement; self }
	pub fn get_placement(&self) ->             Placement { self.placement }
	pub fn insert_resource(&mut self, name: String, resource: Resource) -> &mut Self { self.resources.insert(name, resource); self }
	pub fn get_resources(&self) -> &HashMap<String, Resource> { &self.resources }
	pub fn get_resources_mut(&mut self) -> &mut HashMap<String, Resource> { &mut self.resources }
	pub fn get_mut_resource(&mut self, name: &String) -> Option<&mut Resource> { self.resources.get_mut(name) }
	pub fn push_ability(&mut self, ability: Rc<RefCell<Box<ability::Ability>>>) -> &mut Self { self.abilities.push(ability); self }
	pub fn get_abilities(&self) -> &Vec<Rc<RefCell<Box<ability::Ability>>>> { &self.abilities }
	pub fn get_attributes(&self) -> &HashSet<String> { &self.attributes }
	pub fn set_attributes(&mut self, attributes: HashSet<String>) -> &mut Self { self.attributes = attributes; self }

	pub fn path_matches(&self, path: &String) -> bool{
		return path == &self.path
	}
	pub fn reposition(&mut self, to: game::Position) -> Result<(), &'static str> {
		match self.placement {
			Placement::Position(_) => {
				self.placement = Placement::Position(to);
				Ok(())
			},
			_ => Err("not positioned")
		}
	}
}

#[derive(Serialize, Deserialize, Clone, Copy, PartialEq, Debug)]
pub enum Placement {
	Position(game::Position),
	OutOfPlay,
	Unplayed,
//	Environment,
}

