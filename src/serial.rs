use serde_derive::{Serialize, Deserialize};
use std::collections::{HashMap, HashSet};
use std::io::prelude::*;
use std::rc::Rc;
use std::cell::RefCell;
use super::*;

// TODO: custom serde impementations
#[cfg(test)]
pub mod serial_tests {
	use super::*;
	#[test]
	fn make() {
		let p = piece::piece_tests::make_pawn();
		let sp = SerialPiece::from_real(&p.borrow());
		let k = piece::piece_tests::make_main();
		let sk = SerialPiece::from_real(&k.borrow());
		let mut kit = Kit::new();
		kit.insert_piece(&sp.0, sp.1);
		kit.insert_piece(&sk.0, sk.1);
		kit.push_path(&sp.0);
		kit.push_path(&sk.0);
		kit.set_title(&"jaxter".to_string());
		kit.save(&"test.json".to_string()).expect("could not save file");
		kit.set_title(&"other".to_string());
		kit.save(&"test2.json".to_string()).expect("could not save file");
	}
}
#[derive(Serialize, Deserialize, Clone, PartialEq)]
pub struct SerialPiece {
	name: String,
	rank: u8,
	attributes: HashSet<String>,
	abilities: Vec<ability::Ability>,
	placement: piece::Placement,
	resources: HashMap<String, ability::Resource>,
}
impl SerialPiece {
	pub fn new() -> Self { SerialPiece{name: "".to_string(), rank: 0, attributes: HashSet::new(), abilities: vec![], placement: piece::Placement::Unplayed, resources: HashMap::new()} }
	pub fn set_name(&mut self, name: String) -> &mut Self { self.name = name; self }
	pub fn push_ability(&mut self, ability: ability::Ability) -> &mut Self { self.abilities.push(ability); self }
	pub fn set_placement(&mut self, placement: piece::Placement) -> &mut Self { self.placement = placement; self }
	pub fn set_rank(&mut self, rank: u8) -> &mut Self { self.rank = rank; self }
	pub fn set_attributes(&mut self, attributes: HashSet<String>) -> &mut Self { self.attributes = attributes; self }
	pub fn insert_resource(&mut self, name: String, resource: ability::Resource) -> Option<ability::Resource> { self.resources.insert(name, resource) }

	pub fn to_real(&self, path: &String) -> Rc<RefCell<Box<piece::Piece>>> {
		let mut result = Rc::new(RefCell::new(Box::new(piece::Piece::new())));
		result.borrow_mut()
			.set_name(self.name.clone())
			.set_path(path.clone())
			.set_placement(self.placement)
			.set_rank(self.rank)
			.set_attributes(self.attributes.clone());
		for ea_ability in &self.abilities {
			let ability = Rc::new(RefCell::new(Box::new(ea_ability.clone())));
			ability.borrow_mut()
//			.set_self_ref(&ability)
			.set_owner(&result)
			;
			result.borrow_mut().push_ability(ability);
		}
		for (ea_name, ea_resource) in self.resources.iter() {
			result.borrow_mut().insert_resource(ea_name.clone(), ea_resource.clone());
		}
		result
	}
	pub fn from_real(real: &piece::Piece) -> (String, SerialPiece) {
		let mut result = SerialPiece::new();
		result
			.set_name(real.get_name().clone())
			.set_placement(real.get_placement().clone())
			.set_rank(*real.get_rank())
			.set_attributes(real.get_attributes().clone());
		for ea_ability in real.get_abilities() {
			result.push_ability(*ea_ability.borrow().clone());
		}
		for (ea_path, ea_resource) in real.get_resources() {
			result.insert_resource(ea_path.clone(), ea_resource.clone());
		}
		( real.get_path().clone(), result )
	}
}
/*
#[derive(Serialize, Deserialize, Clone, PartialEq)]
pub struct SerialGame {
	teams: Vec<SerialTeam>,
	turn: i64,
	phase: game::Phase,
	board: Vec<game::Position>,
}*/

#[derive(Serialize, Deserialize, Clone, PartialEq)]
pub struct SerialPlayer {
	name: String,
	position: i32,
	abilities: Vec<ability::Ability>,
	pieces: Vec<SerialPiece>,
	resources: HashMap<String, ability::Resource>,
}

#[derive(Serialize, Deserialize, Clone, PartialEq)]
pub struct SerialTeam {
	players: Vec<SerialPlayer>,
}

#[derive(Serialize, Deserialize, Clone, PartialEq)]
pub struct Kit {
	title: String,
	authors: Vec<String>,
	paths: Vec<String>,
	pool: HashMap<String, SerialPiece>,
}

impl std::fmt::Display for Kit {
	fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
		write!(f, "Kit: {}\n", self.title)?;
		for ea_piece in &self.paths {
			println!("{}", ea_piece);
		}
		Ok(())
	}
}
impl Kit {
	pub fn new() -> Self { Kit{title: "".to_string(), authors: vec![], paths: vec![], pool: HashMap::new()} }
	pub fn set_title(&mut self, title: &String) -> &mut Self { self.title = title.clone(); self }
	pub fn push_path(&mut self, path: &String) { self.paths.push(path.clone()); }
	pub fn contains_piece(&self, path: &String) -> bool { self.pool.contains_key(path) }
	pub fn get_title(&self) -> &String { &self.title }
	pub fn insert_piece(&mut self, path: &String, piece: SerialPiece) -> Option<SerialPiece> {
		if self.pool.contains_key(path) {
			// TODO: throw error
			None
		}
		else {
			self.pool.insert(path.clone(), piece)
		}
	}

	pub fn to_pieces(&self) -> Vec<Rc<RefCell<Box<piece::Piece>>>> {
		let mut pieces: Vec<Rc<RefCell<Box<piece::Piece>>>> = vec![];
		for ea_path in &self.paths {
			let ea_piece = self.pool.get(ea_path).expect("Path not found in pool").to_real(ea_path);
			pieces.push(ea_piece);
		}
		pieces
	}

	pub fn load(filename: &String) -> Result<Self, &'static str> {
		let mut file = std::fs::File::open(&filename).expect("Error opening kit file");
		let mut text = String::new();
		file.read_to_string(&mut text).expect("Error reading kit file");
		let out = serde_json::from_str(&text).expect("Error deserializing kit file");
		Ok(out)
	}

	pub fn save(&self, filename: &String) -> Result<(), &'static str> {
		let mut file = std::fs::File::create(filename)
			.expect("Error opening kit");
		file.write_all( serde_json::to_string_pretty(self)
			.expect("Error serializing kit").as_bytes()
		).expect("Error writing kit");
		Ok(())
	}

	pub fn to_string(&self) -> Result<String, serde_json::error::Error> {
		serde_json::to_string_pretty(self)
	}

	pub fn append_piecepool(&mut self, add: &Kit) -> HashMap<String, Option<SerialPiece>> {
		let mut output = HashMap::new();
		for (ea_path, ea_piece) in add.pool.iter() {
			output.insert(ea_path.clone(), self.insert_piece(ea_path, ea_piece.clone()));
		}
		output
	}

	pub fn get_piece(&self, path: &String) -> Option<&SerialPiece> {
		self.pool.get(path)
	}
}

