//! Structures pertaining to game state
use serde_derive::{Serialize, Deserialize};
use std::collections::HashMap;
use std::collections::HashSet;
use std::hash::{Hash, Hasher};
use std::collections::VecDeque;
//use itertools::Itertools;
use std::rc::Rc;
use std::cell::RefCell;
use super::*;

#[cfg(test)]
mod game_state_tests {
	use super::*;

	#[test]
	fn make() {
		let rcrc_g = Game::make_1v1_game();
		let mut game = rcrc_g.borrow_mut();
		let p = piece::piece_tests::make_main();
		let p1 = piece::piece_tests::make_main();
//		let p2 = piece::piece_tests::make_pawn();
		game.get_players().get(0).unwrap().borrow_mut().push_piece(p.clone());
		game.get_players().get(1).unwrap().borrow_mut().push_piece(p1.clone());
//		p.borrow_mut().set_placement(piece::Placement::Position(Position::new(5,8)));
//		game.get_choices();
		// player chooses
		// resolve choice
		// get list of choices
		game.generate_choice_tree();
		let t = game.get_choice_tree();
		for ea_act in t.get().keys() {
//			println!("{:?}", ea_act);
		}
		let k = Action::PlacePiece(p.clone());
		t.get().get(&k);
//		for ea_op in t.get_next_options(&(Some(k), VecDeque::new())).expect("poop") {
//			match ea_op {
//				Choice::Action(a) => {
//					println!("{:?}", a);
//				},
//				Choice::Target(t) => {
//					
//				}
//			}
//		}
//		println!("{}", game.get_choices().to_string());
//		game.next(false);
//		game.activate_mandatory();
//		game.resolve_stack();
//		println!("{}", game.to_string_pieces());
//		game.next(false);
//		println!("{}", game.to_string_pieces());
	}
}

#[derive(Serialize, Deserialize, Clone, PartialEq, Eq, Hash, Debug)]
pub enum Direction {
	NE,
	E,
	SE,
	SW,
	W,
	NW,
}

#[derive(Serialize, Deserialize, Clone, PartialEq, Eq, Debug)]
pub struct Cell {
	players: HashSet<usize>,
	directions: HashSet<Direction>,
}
impl Cell {
	pub fn new() -> Cell { Cell {players: HashSet::new(), directions: HashSet::new()} }
	pub fn insert_direction(&mut self, direction: Direction) -> &mut Self { self.directions.insert(direction); self }
	pub fn insert_player(&mut self, player: usize) -> &mut Self { self.players.insert(player); self }
	pub fn contains_player(&self, player: &usize) -> bool { self.players.contains(player) }
	pub fn get_directions(&self) -> &HashSet<Direction> { &self.directions }
	pub fn get_players(&self) -> &HashSet<usize> { &self.players }
}

#[derive(Clone, PartialEq, Debug)]
pub enum Action {
	Pass,
	End,
	FinishPlacing,
	PlacePiece(Rc<RefCell<Box<piece::Piece>>>),
//	UseAbility(Rc<RefCell<Box<ability::Ability>>>),
}
impl Eq for Action {}
impl Hash for Action {
	fn hash<H: Hasher>(&self, state: &mut H) {
		0.hash(state);
//		match self {
//			Action::Pass => 0.hash(state),
//			Action::End => 1.hash(state),
//			Action::FinishPlacing => 2.hash(state),
//			Action::PlacePiece(r) => r.as_ptr().hash(state),
//			Action::UseAbility(r) => r.as_ptr().hash(state),
//			Action::PlacePiece(r) =>3.hash(state),
//			Action::UseAbility(r) =>4.hash(state),
//		};
	}
}


pub enum Choice {
	Action(Action),
	Target(ability::Target),
}

//pub struct ChoiceTree(pub HashMap<Action, TargetTree>);
//use std::collections::hash_map::Keys;
#[derive(Clone, PartialEq, Debug)]
pub struct ChoiceTree {
	s: HashMap<Action, TargetTree>
}
impl ChoiceTree {
	pub fn new() -> Self { ChoiceTree{s: HashMap::new()} }
	pub fn get(&self) -> &HashMap<Action, TargetTree> { &self.s }
	pub fn insert(&mut self, key: Action, value: TargetTree) -> Option<TargetTree> { self.s.insert(key, value) }
	pub fn insert_action(&mut self, action: Action) -> Option<TargetTree> {
		self.s.insert(action, TargetTree::new())
	}
//	pub fn get(&self) -> &Vec<TargetTree> { &self.next }
	pub fn has_branches(&self) -> bool {
		self.s.len() > 0
	}
	pub fn get_next_options(&self, decision: &(Option<game::Action>, VecDeque<ability::Target>))
			-> Result<Vec<Choice>, &'static str> {
		let mut result = Vec::new();
		match decision.0.clone() {
			None => {
				for ea_action in self.s.keys() {
					result.push(Choice::Action(ea_action.clone()));
				}
				Ok(result)
			},
			Some(d) => {
				match self.get().get(&d) {
					Some(t) => {
						t.get_next_options(decision.1.clone())
					},
					None => Err("Invalid action decision"),
				}
			},
		}
	}

	/// Generate a choice tree based on the current game state. This method decides what is possible at the game level.
	pub fn generate(game: &Game) -> Self {
		let priority_player = game.get_players().get(*game.get_priority()).expect("Priority player not found").clone();
		let mut result = ChoiceTree::new();
		// if a choice tree doesnt exint, build a new one
		match priority_player.borrow().get_done_placing() {
			false => {
				// Placement phase
				for ea_piece in priority_player.borrow().get_pieces() {
					match &ea_piece.borrow().get_placement() {
						piece::Placement::Unplayed => {
							let mut ttree = TargetTree::new();
							for ea_pos in game.get_placeable_positions(&ea_piece.borrow()) {
								ttree.insert_target(ability::Target::Position(ea_pos));
							}
							if ttree.has_branches() { // If piece can be placed
								result.insert(Action::PlacePiece(ea_piece.clone()), ttree);
							}
						},
						_ => (), // ignore played pieces
					}
				}
				result.insert_action(Action::End);
			},
			true => {
				// gameplay logic
				for ea_piece in priority_player.borrow().get_pieces() {
					match &ea_piece.borrow().get_placement() {
						piece::Placement::Position(_) => {
//							for ea_ability in ea_piece.borrow().get_abilities() {
//								match ea_ability.borrow().check(&mut VecDeque::new(), false, game) {
//									_ => (),//do stuff
//								}
//							}

						},
						_ => (),
					}
				}
				result.insert_action(Action::Pass);
			}
		}
		result
	}
}

//#[derive(Clone, PartialEq)]
//pub struct ActionTree {
//	action: Action,
//	next: Vec<TargetTree>,
//}
//impl ActionTree {
//	pub fn new(action: Action) -> Self {
//		ActionTree{action, next: Vec::new()}
//	}
//	pub fn push_target(&mut self, target: ability::Target) {
//		self.next.push(TargetTree::new(target))
//	}
//	pub fn get_action(&self) -> &Action { &self.action }
//	pub fn has_branches(&self) -> bool {
//		self.next.len() > 0
//	}
//}

// should this be part of the server or the client?
// since the server has to check validity anyway, might as well make the server generate choices too
#[derive(Clone, PartialEq, Debug)]
pub struct TargetTree {
	s: HashMap<ability::Target, TargetTree>
}
impl TargetTree {
	pub fn new() -> Self { TargetTree{s: HashMap::new()} }
	pub fn get(&self) -> &HashMap<ability::Target, TargetTree> { &self.s }
	pub fn insert(&mut self, key: ability::Target, value: TargetTree) -> Option<TargetTree> { self.s.insert(key, value) }
	pub fn insert_target(&mut self, target: ability::Target) -> Option<TargetTree> { self.s.insert(target, TargetTree::new()) }
//	pub fn get(&self) -> &Vec<TargetTree> { &self.next }
	pub fn has_branches(&self) -> bool { self.s.len() > 0 }
	pub fn get_next_options(&self, mut decision: VecDeque<ability::Target>)
			-> Result<Vec<Choice>, &'static str> {
		let mut result = Vec::new();
		match decision.pop_front() {
			Some(d) => {
				match self.s.get(&d) {
					Some(t) => {
						t.get_next_options(decision)
					}
					None => Err("Invalid decision"),
				}
			},
			None => {
				for ea_target in self.s.keys() {
					result.push(Choice::Target(ea_target.clone()));
				}
				Ok(result)
			},
		}
	}}

#[derive(Clone, PartialEq, Debug)]
pub struct Board {
	cells: HashMap<Position, Cell>,
//	minimum: (isize, isize),
//	size: (isize, isize),
}
impl Board {
	pub fn new() -> Self { Board{cells:HashMap::new()} }
	pub fn set(&mut self, to: HashMap<Position, Cell>) { self.cells = to; }
	pub fn get(&self) -> &HashMap<Position, Cell> { &self.cells }
	pub fn get_minimum(&self) -> (isize, isize) {
		let mut min_x = std::isize::MAX;
		let mut min_y = std::isize::MAX;
		for ea_pos in self.cells.keys() {
			if ea_pos.get_x() < min_x { min_x = ea_pos.get_x() }
			if ea_pos.get_y() < min_y { min_y = ea_pos.get_y() }
		}
		(min_x, min_y)
	}
	pub fn get_bounds(&self) -> ((isize, isize), (isize, isize)) {
		let mut min_x = std::isize::MAX;
		let mut min_y = std::isize::MAX;
		let mut max_x = std::isize::MIN;
		let mut max_y = std::isize::MIN;
		for ea_pos in self.cells.keys() {
			if ea_pos.get_x() < min_x { min_x = ea_pos.get_x() }
			if ea_pos.get_y() < min_y { min_y = ea_pos.get_y() }
			if ea_pos.get_x() > max_x { max_x = ea_pos.get_x() }
			if ea_pos.get_y() > max_y { max_y = ea_pos.get_y() }
		}
		((min_x, min_y), (max_x-min_x+1, max_y-min_y+1))
	}

	/// Create a annulus board with a given inner radius and thickness
	pub fn new_annulus_board(inner_r: usize, thickness: usize) -> Self {
		let mut cells = HashMap::new();
		let outer_r = (inner_r + thickness) as isize;
		for ea_x in -outer_r..=outer_r {
			for ea_y in (-outer_r..=outer_r).rev() {
					if //ea_x.abs() <= outer_r && ea_y.abs() <= outer_r && // implied by initial bounds
						(ea_x + ea_y).abs() <= outer_r &&
						!(	(ea_x.abs() as usize) < inner_r &&
							(ea_y.abs() as usize) < inner_r &&
							((ea_x + ea_y).abs() as usize) < inner_r	) {
						let mut cell = Cell::new();
						if ea_y >= 0 && ea_x > 0       { cell.insert_direction(Direction::NW); }
						if ea_y <= 0 && ea_x < 0       { cell.insert_direction(Direction::SE); }
						if ea_y < 0  && ea_x+ea_y >= 0 {
							cell.insert_direction(Direction::W);
							cell.insert_player(1);
						}
						if ea_y > 0  && ea_x+ea_y <= 0 {
							cell.insert_direction(Direction::E);
							cell.insert_player(0);
						}
						if ea_x >= 0 && ea_x+ea_y < 0  { cell.insert_direction(Direction::SW); }
						if ea_x <= 0 && ea_x+ea_y > 0  { cell.insert_direction(Direction::NE); }
						if ea_x == 0 &&      ea_y == 0 {
							cell.insert_direction(Direction::NE);
							cell.insert_direction(Direction::E);
							cell.insert_direction(Direction::SE);
							cell.insert_direction(Direction::SW);
							cell.insert_direction(Direction::W);
							cell.insert_direction(Direction::NW);
						}
						cells.insert(Position::new(ea_x, ea_y), cell);
					}
			}
		}
		Board{cells}
	}
}

#[derive(Clone, PartialEq, Debug)]
pub struct Game {
	self_ref: Option<Rc<RefCell<Box<Self>>>>,
	// Time
	turn: i64,
	turn_player: usize,
	phase: Phase,
	priority: usize,
	is_end_of_phase: bool,
	// Game state
	players: Vec<Rc<RefCell<Box<Player>>>>,
	queued_actions: VecDeque<ability::Effect>,
	board: Board,
//	current_choice: {choice: Option<Choice>, VecDeque<ability::Target>},
	// Temp
	choice_tree: ChoiceTree,
}
impl Game {
	pub fn new() -> Rc<RefCell<Box<Self>>> {
		let game = Rc::new(RefCell::new(Box::new(Game {
			self_ref: None, players: vec![],
			turn: 0, turn_player: 0, phase: Phase::Start, priority: 0, is_end_of_phase: false,
			queued_actions: VecDeque::new(), choice_tree: ChoiceTree::new(), board: Board::new(),
		})));
		game.borrow_mut().self_ref = Some(game.clone());
		game
	}
	pub fn push_player(&mut self, player: Rc<RefCell<Box<Player>>>) -> &mut Self {
		player.borrow_mut().set_owner(self.self_ref.as_ref().unwrap().clone());
		self.players.push(player);
		self
	}
	pub fn get_players(&self) -> &Vec<Rc<RefCell<Box<Player>>>> { &self.players }
	pub fn get_priority(&self) -> &usize { &self.priority }
	pub fn get_current_turn_player(&self) -> &Rc<RefCell<Box<Player>>> {
		self.players.get(self.turn_player).expect("Could not find current player")
	}
	pub fn get_current_priority_player(&self) -> &Rc<RefCell<Box<Player>>> {
		self.players.get(self.priority).expect("Could not find current player")
	}
	pub fn get_pieces(&self) -> Vec<Rc<RefCell<Box<piece::Piece>>>> {
		let mut result = Vec::new();
		for ea_player in &self.players {
				result.append(&mut ea_player.borrow().get_pieces().clone());
		}
		result
	}
//	pub fn push_piece(&mut self, piece: Rc<RefCell<Box<piece::Piece>>) -> &mut Self { self.pieces.push(piece); self }
	pub fn set_board(&mut self, board: Board) -> &mut Self { self.board = board; self }
	pub fn get_board(&self) -> &Board { &self.board }
	pub fn generate_choice_tree(&mut self) {self.choice_tree = ChoiceTree::generate(self);}

	/// End current player's priority by incrementing the acting player to the next player
	pub fn set_next_player(&mut self) {
		let mut end_placement_phase = true;
		for _ in 0..self.players.len() {
			self.priority += 1;
			if self.priority >= self.players.len() {self.priority = 0; }
//			println!("testing player {}", self.priority);
			if !self.get_current_priority_player().borrow().get_done_placing() {
//				println!("unplaced player found {}", self.priority);
				end_placement_phase = false;
				break;
			}
		}
		if end_placement_phase {
			self.priority = self.turn_player;
		}
	}
	pub fn get_turn(&self) -> &i64 { &self.turn }
	pub fn get_phase(&self) -> &Phase { &self.phase }
	pub fn is_end_of_phase(&self) -> &bool { &self.is_end_of_phase }
	pub fn next(&mut self, cycle: bool, num_players: usize) {
		self.is_end_of_phase = match self.is_end_of_phase {
			true => {
				self.phase = match self.phase {
					Phase::Start => Phase::Move,
					Phase::Move => Phase::Attack,
					Phase::Attack => if cycle {
						Phase::Move
					}
					else {
						Phase::End
					},
					Phase::End => {
						self.turn_player += 1;
						if self.turn_player >= num_players {
							self.turn_player = 0
						}
						self.turn += 1;
						Phase::Start
					},
				};
				false
			},
			false => true,
		};
	}

//	pub fn pieces_to_string(&self) -> String {
//		let mut result = String::new();
//		for ea_p in &self.players {
//			let ea_player = ea_p.borrow();
//			result.push_str(ea_player.get_name());
//			result.push_str(":\n");
//			for ea_piece in ea_player.get_pieces() {
//				result.push_str(ea_piece.borrow_mut().get_name());
//				result.push('\n');
//			}
//		}
//		result
//	}

	pub fn get_choice_tree(&self) -> &ChoiceTree { &self.choice_tree }

	/// Add choice to the queue of choices. If the added choice is Choice::End, then all
	/// choices in queue will be resolved.
	pub fn resolve(&mut self, action: Action, mut targets: VecDeque<ability::Target>) {
		// TODO: verify choice
		match &action {
			Action::End => {
				println!("end phase");
				self.get_current_priority_player().borrow_mut().set_done_placing(true);
				self.set_next_player();
			},
			Action::PlacePiece(piece) => match targets.pop_front().expect("Not enough arguments") {
				ability::Target::Position(pos) => {
					piece.borrow_mut().set_placement(piece::Placement::Position(pos));
					// consider skipping players with no placeable pieces
					self.set_next_player();
				},
				_ => panic!("Wrong argument"),
			},
			Action::Pass => {
				self.priority += 1;
				if self.priority >= self.players.len() { self.priority = 0; }
			},
			_ => unimplemented!(),
		}
		if targets.len() != 0 {
			panic!("Too many arguments")
		}
		self.choice_tree = ChoiceTree::new();
	}

	/// Preset for making a generic 1v1 game. Should be removed by 1.0.0.
	pub fn make_1v1_game() -> Rc<RefCell<Box<Game>>> {
		let rcrc_g = Game::new();
		{
			let mut game = rcrc_g.borrow_mut();
			game.set_board(Board::new_annulus_board(2,5));
			let p1 = Player::new();
			p1.borrow_mut().set_name("Player 1".to_string()).set_source(DecisionSource::Client);
			let p2 = Player::new();
			p2.borrow_mut().set_name("Player 2".to_string()).set_source(DecisionSource::Client);
			game.push_player(p1);
			game.push_player(p2);
		}
		rcrc_g
	}

	/// Get list of positions where a given piece can be placed
	pub fn get_placeable_positions(&self, piece: &piece::Piece) -> Vec<Position> {
		let mut result = Vec::new();
		match &piece.get_rank() {
			// Main pieces can be placed anywhere in a given space (determined by board)
			0 => for (ea_pos, ea_cell) in self.get_board().get().iter() {
				let x_pos = ea_pos.get_x();
				let y_pos = ea_pos.get_y();
				if ea_cell.contains_player(self.get_priority()) &&
				self.is_open_cell(Position::new(x_pos, y_pos)) {
					result.push(*ea_pos);
				}
			},
			// Non-main pieces (pieces with rank >= 1) have to be placed next to
			// a higher ranking piece of the same color
			rank => {
				let mut possible_targets = HashSet::new();
				// get every piece placed by currently placing player
				for ea_piece in self.get_current_priority_player().borrow().get_pieces() {
					if ea_piece.borrow().get_rank() < rank &&
					 ( piece.get_attributes().len() == 0 ||
					   ea_piece.borrow().get_attributes().union(piece.get_attributes()).collect::<Vec<&String>>().len() >= 1 ) {
						match ea_piece.borrow().get_placement() {
							piece::Placement::Position(p) => {
								// add every cell surrounding each piece
								for ea_target in self.expand_range(p, Range::new((0,0),1)) {
									possible_targets.insert(ea_target);
								}
							}
							_ => ()
						}
					}
				}
				for ea_pos in self.is_open_cell_batch(possible_targets) {
					result.push(ea_pos);
				}
			},
		};
		result
	}


	/// Check to see if cell exists on the board at a given position and
	/// is not occupied by a piece. Generally used for checking move
	/// validity.
	pub fn is_open_cell(&self, position: Position) -> bool {
		if self.board.get().get(&position).is_some() {
			for ea_piece in self.get_pieces() {
				match ea_piece.borrow().get_placement() {
					piece::Placement::Position(p) => {
						if p == position {
							return false;
						}
					},
					_ => (),
				}
			}
			return true;
		}
		false
	}

	/// Slightly faster batch version of is_open_cell()
	pub fn is_open_cell_batch(&self, position: HashSet<Position>) -> HashSet<Position> {
		// make temporary map of locations with pieces
		let mut piece_map = HashSet::new();
		for ea_player in &self.players {
			for ea_piece in ea_player.borrow().get_pieces() {
				match ea_piece.borrow().get_placement() {
					piece::Placement::Position(p) => {
						piece_map.insert(p);
					},
					_ => (),
				}
			}
		}
		let test = self.board.get().keys().cloned().collect();
		position
		.intersection(&test).cloned().collect::<HashSet<Position>>()
		.difference(&piece_map).cloned().collect::<HashSet<Position>>()
	}

	/// Converts a range into a list of valid positions that it can target
	pub fn expand_range(&self, origin: Position, range: Range) -> HashSet<Position> {
		let mut result = HashSet::new();
		// TODO deal with line of sight
		if range.get_direction() == Position::new(0,0) {
			match range.get_distance() {
				Some(raw_d) => {
					let d = raw_d as isize;
					// get all positions in radius d
					for ea_x in -d..=d {
						for ea_y in -d..=d {
							if (ea_x + ea_y).abs() <= d {
								let pos = origin + Position::new(ea_x, ea_y);
								// filter out nonexistent spaces
								if self.board.get().get(&pos).is_some() {
									result.insert(pos);
								}
							}
						}
					}
				},
				None => {
					for ea_pos in self.board.get().keys() {
						result.insert(*ea_pos);
					}
				},
			}

		}

//		result.push(Position::new(self+Direction::new(1,1)));
		result
	}
}

#[derive(Serialize, Deserialize, Clone, Copy, PartialEq, Debug)]
pub enum Phase {
	Start,
	Move,
	Attack,
	End,
}
impl Phase {
	pub fn to_string(&self) -> String {
		match self {
			Phase::Start => "Start".to_string(),
			Phase::Move => "Move".to_string(),
			Phase::Attack => "Attack".to_string(),
			Phase::End => "End".to_string(),
		}
	}
}

#[derive(Clone, PartialEq, Debug)]
/// Where a player gets its move choices from.
pub enum DecisionSource {
	None,
	Random,
	Computer,
	Client,
	Remote,
//	Other(String),
}

#[derive(Clone, PartialEq, Debug)]
pub struct Player {
	self_ref: Option<Rc<RefCell<Box<Player>>>>,
	owner: Option<Rc<RefCell<Box<Game>>>>,
	source: DecisionSource,
	name: String,
	team: usize,
	done_placing: bool,
	abilities: Vec<Rc<RefCell<Box<ability::Ability>>>>,
	pieces: Vec<Rc<RefCell<Box<piece::Piece>>>>,
	resources: HashMap<String, ability::Resource>,
}
impl Player {
	pub fn new() -> Rc<RefCell<Box<Player>>> {
		let t = Rc::new(RefCell::new(Box::new(Player {
			self_ref: None, owner: None, name: "".to_string(), team: 0, source: DecisionSource::None, done_placing: false,
			abilities: vec![], pieces: vec![], resources: HashMap::new(),
		})));
		t.borrow_mut().self_ref = Some(t.clone());
		t
	}
	pub fn push_piece(&mut self, piece: Rc<RefCell<Box<piece::Piece>>>) -> &mut Self { self.pieces.push(piece); self }
	pub fn set_owner(&mut self, owner: Rc<RefCell<Box<Game>>>) -> &mut Self { self.owner = Some(owner); self }
	pub fn set_name(&mut self, name: String) -> &mut Self { self.name = name; self }
	pub fn get_name(&self) -> &String { &self.name }
	pub fn get_pieces(&self) -> &Vec<Rc<RefCell<Box<piece::Piece>>>> { &self.pieces }
	pub fn set_pieces(&mut self, pieces: Vec<Rc<RefCell<Box<piece::Piece>>>>) -> &mut Self { self.pieces = pieces; self }
	pub fn get_pieces_mut(&mut self) -> &mut Vec<Rc<RefCell<Box<piece::Piece>>>> { &mut self.pieces }
	pub fn get_done_placing(&self) -> bool { self.done_placing }
	pub fn set_done_placing(&mut self, done_placing: bool) -> &mut Self { self.done_placing = done_placing; self}
	pub fn set_source(&mut self, source: DecisionSource) -> &mut Self { self.source = source; self }
	pub fn get_source(&self) -> &DecisionSource { &self.source }
}

#[derive(Serialize, Deserialize, Copy, Clone, PartialEq, Eq, Hash, Debug)]
pub struct Range {
	line_of_sight: bool,
	direction: Position,
	distance: Option<usize>,
}
impl Range {
	pub fn new(direction: (isize, isize), distance: usize) -> Self { Range{line_of_sight:false, direction:Position::new(direction.0, direction.1), distance:Some(distance)} }
	pub fn set_distance(&mut self, distance: Option<usize>) -> &mut Self { self.distance = distance; self }
	pub fn set_line_of_sight(&mut self, line_of_sight: bool) -> &mut Self { self.line_of_sight = line_of_sight; self }
	pub fn set_direction(&mut self, direction: Position) -> &mut Self { self.direction = direction; self }
	pub fn get_distance(&self) -> Option<usize> { self.distance }
	pub fn get_line_of_sight(&self) -> bool { self.line_of_sight }
	pub fn get_direction(&self) -> Position { self.direction }
}
#[derive(Serialize, Deserialize, Copy, Clone, PartialEq, Eq, Hash, Debug)]
pub struct Position {
	x: isize,
	y: isize,
}
impl Position {
	pub fn new(x: isize, y: isize) -> Position { Position{x,y} }
	pub fn get_x(&self) -> isize { self.x }
	pub fn get_y(&self) -> isize { self.y }
//	pub fn is_valid(&self) -> bool { self.x.is_ok() && self.y.is_ok() }


	pub fn check_range(&mut self, _range: Range, _game: &game::Game) -> bool {
/*		let in_range: bool;
		if range.0.x == 0 && range.0.y == 0 { // radial range
			in_range = match range.1 {
				Some(limit) => {
					max(self.x, self.y) < limit && self.x + self.y < limit
			    }
			    None => true
			};
		}
		else {
			in_range = match self.is_direction(range.0) {
				true => {
						match range.1 {
							Some(limit) => {
								self.x / range.0.x < limit
						    }
						    None => true
						}
				},
				false => false,
			};
		}
		if !in_range {
			false
		}
		else if !line_of_sight {
			true
		}
		else if range.0.x == 0 && range.0.y == 0 {
			//TODO: implement radial sight blocking
			match range.1 {
				Some(limit) => {
					max(self.x, self.y) < limit && self.x + self.y < limit
			    },
			    None => true // Infinite range
			}
		}
		else {
			match self.is_direction(range.0) {
				true => {
					match range.1 {
						Some(limit) => {
							let mut check = Position{x:0,y:0};
							let mut out = true;
							for _ in 0..limit {
								check += range.0;
								if game.is_open_cell(check) {
									out = false;
									break;
								}
							}
							out
					    }
					    None => true
					}
				},
				false => false,
			}
		}
*/true
	}

	pub fn is_range(&self, _range: Range) -> bool {
/*		if !self.is_valid() {
			return false;
		}
		if direction.x == 0 && direction.y == 0 {
			// If it's radial, it covers every direction
			true
		}
		else {
			// Otherwise, direction has to be a factor of self
			((direction.x == 0) || ((self.x % direction.x) == 0)) &&
			((direction.y == 0) || ((self.y % direction.y) == 0)) &&
			((self.x / direction.x) ==
			 (self.y / direction.y))
		}
	*/true
	}
}
impl std::ops::Add for Position {
	type Output = Self;
	fn add(self, other: Self) -> Self {
		Self {
			x: self.x - other.x,
			y: self.y - other.y,
		}
	}
}
impl std::ops::Sub for Position {
	type Output = Self;
	fn sub(self, other: Self) -> Self {
		Self {
			x: self.x - other.x,
			y: self.y - other.y,
		}
	}
}
impl std::ops::AddAssign for Position {
	fn add_assign(&mut self, other: Self) {
		*self = Self {
			x: self.x + other.x,
			y: self.y + other.y,
		};
	}
}
impl std::ops::SubAssign for Position {
	fn sub_assign(&mut self, other: Self) {
		*self = Self {
			x: self.x - other.x,
			y: self.y - other.y,
		};
	}
}

