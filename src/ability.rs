use serde_derive::{Serialize, Deserialize};
use std::collections::HashMap;
use std::hash::{Hash, Hasher};
use std::rc::Rc;
use std::cell::RefCell;
use super::*;

// AoE damage
// Command: Prevent a target's movement
// Basic attack
// Aura: reduce damage of all enemy pieces in a 2 cell radius
#[derive(Serialize, Deserialize, Clone, PartialEq, Debug)]
pub struct Ability {
	#[serde(skip)]
	owner: Option<Rc<RefCell<Box<piece::Piece>>>>,
//	#[serde(skip)]
//	self_ref: Option<Rc<RefCell<Box<Self>>>>,
	name: String,
	trigger: Option<Trigger>,
	activation_condition: Option<Condition>,
	effect: Effect,
	resources: HashMap<String, Resource>,
}
impl Ability {
	pub fn new() -> Rc<RefCell<Box<Self>>> {
		let result = Rc::new(RefCell::new(Box::new(Ability{
//			self_ref:None,
			owner: None,
			name: "".to_string(),
			trigger: None, activation_condition: None, effect: Effect::DoNothing, resources: HashMap::new()})));
//		result.borrow_mut().self_ref = Some(result.clone());
		result
	}
	pub fn set_name(&mut self, name: String) -> &mut Self { self.name = name; self }
	pub fn set_trigger(&mut self, trigger: Option<Trigger>) -> &mut Self { self.trigger = trigger; self }
	pub fn set_activation_condition(&mut self, activation_condition: Option<Condition>) -> &mut Self { self.activation_condition = activation_condition; self }
	pub fn set_effect(&mut self, effect: Effect) -> &mut Self { self.effect = effect; self }
//	pub fn set_self_ref(&mut self, self_ref: &Rc<RefCell<Box<Self>>>) -> &mut Self { self.self_ref = Some(self_ref.clone()); self }
	pub fn set_owner(&mut self, owner: &Rc<RefCell<Box<piece::Piece>>>) -> &mut Self { self.owner = Some(owner.clone()); self }
	pub fn get_owner(&self) -> &Rc<RefCell<Box<piece::Piece>>> { &self.owner.as_ref().unwrap() }
	pub fn insert_resource(&mut self, name: String, resource: Resource) -> Option<Resource> { self.resources.insert(name, resource) }
//	fn push_status(&self, status: Status) {}
	pub fn check(&self, source: &Rc<RefCell<Box<Self>>>, targets: &mut VecDeque<Target>, activate: bool, game: &game::Game) -> ConditionResult {
		self.effect.check(source, targets, activate, game)
	}
}

#[derive(Clone, PartialEq, Debug)]
pub enum Target {
	Ability(Rc<RefCell<Box<Ability>>>),
	Piece(Rc<RefCell<Box<piece::Piece>>>),
	Player(Rc<RefCell<Box<game::Player>>>),
	Position(game::Position),
//	CompoundTarget(Vec<Target>),
	Game,
}
impl Eq for Target {}
impl Hash for Target {
	fn hash<H: Hasher>(&self, state: &mut H) {
		match self {
//			Target::Ability(r) => r.as_ptr().hash(state),
//			Target::Piece(r) => r.as_ptr().hash(state),
//			Target::Player(r) => r.as_ptr().hash(state),
			Target::Ability(r) => 1.hash(state),
			Target::Piece(r) => 2.hash(state),
			Target::Player(r) => 3.hash(state),
			Target::Position(p) => p.hash(state),
			Target::Game => 0.hash(state),
		};
	}
}

use std::collections::VecDeque;

#[derive(Serialize, Deserialize, Clone, PartialEq, Debug)]
pub enum TargetAbilityCondition {
	And(Vec<TargetAbilityCondition>),
	Or(Vec<TargetAbilityCondition>),
	Not(Box<TargetAbilityCondition>),
	IsSelf,
	IsReferencedTarget(i32),
	OwnerIs(Box<TargetPieceCondition>),
}
#[derive(Serialize, Deserialize, Clone, PartialEq, Debug)]
pub enum TargetPieceCondition {
	And(Vec<TargetPieceCondition>),
	Or(Vec<TargetPieceCondition>),
	Not(Box<TargetPieceCondition>),
	IsReferencedTarget(i32),
	PositionIs(Box<TargetPositionCondition>),
	HasResource(Box<TargetResourceCondition>),
	IsEnteringBoard,
	Owner(Box<TargetPlayerCondition>),
	IsOwner,
}
impl TargetPieceCondition {
	pub fn check(&self, source: &Rc<RefCell<Ability>>, targets: &VecDeque<Target>, _game: &game::Game) -> ConditionResult {
		let mut targets = (*targets).clone();
		match targets.pop_front() {
			Some(t) => match &t {
				Target::Piece(piece) => match &self {
//					TargetPieceCondition::Owner(check_owner) => check_owner.check(source, targets, game),
					TargetPieceCondition::IsOwner => if Rc::ptr_eq(&piece, source.borrow().get_owner()) {
						ConditionResult::Ok
					}
					else {
						ConditionResult::InvalidTarget
					},
					_ => unimplemented!(),
				},
				_ => ConditionResult::InvalidTargetType,
			},
			None => ConditionResult::InsufficientTargets,
		}
	}
}
#[derive(Serialize, Deserialize, Clone, PartialEq, Debug)]
pub enum TargetPositionCondition {
	Is(game::Position),
	And(Vec<TargetPositionCondition>),
	Or(Vec<TargetPositionCondition>),
	Not(Box<TargetPositionCondition>),
	IsInRange{direction: game::Position, range: Option<usize>, line_of_sight: bool},
	IsUnoccupied,
}
impl TargetPositionCondition {
	pub fn check(&self, _source: &Rc<RefCell<Box<Ability>>>, targets: &VecDeque<Target>, _game: &game::Game) -> ConditionResult {
		let mut targets = targets.clone();
		match targets.pop_front() {
			Some(t) => match t {
				Target::Position(position) => match &self {
					TargetPositionCondition::Is(compare) => if *compare == position {
						ConditionResult::Ok
					}
					else {
						ConditionResult::InvalidTarget
					},
					_ => unimplemented!(),
				},
				_ => ConditionResult::InvalidTargetType,
			},
			None => ConditionResult::InsufficientTargets,
		}
	}
}
#[derive(Serialize, Deserialize, Clone, PartialEq, Debug)]
pub enum TargetResourceCondition {
	And(Vec<TargetResourceCondition>),
	Or(Vec<TargetResourceCondition>),
	Not(Box<TargetResourceCondition>),
	IsNamed(String),
	IsAtLeast(i32),
}
#[derive(Serialize, Deserialize, Clone, PartialEq, Debug)]
pub enum TargetPlayerCondition {
	And(Vec<TargetPlayerCondition>),
	Or(Vec<TargetPlayerCondition>),
	Not(Box<TargetPlayerCondition>),
	IsOwner,
	HasPiece(Box<TargetPieceCondition>),
	IsTeam(Box<TargetTeamCondition>),
}
impl TargetPlayerCondition {
	pub fn check(&self, source: &Rc<RefCell<Ability>>, targets: &VecDeque<Target>, _game: &game::Game) -> ConditionResult {
		let mut targets = targets.clone();
		match targets.pop_front() {
			Some(t) => match t {
				Target::Player(player) => {
					match &self {
						TargetPlayerCondition::IsOwner => if Rc::ptr_eq(&player, source.borrow().get_owner().borrow().get_owner()) {
							ConditionResult::Ok
						}
						else {
							ConditionResult::InvalidTarget
						},
						_ => unimplemented!(),
					}
				},
				_ => ConditionResult::InvalidTargetType
			},
			None => ConditionResult::InsufficientTargets,
		}
	}
}
#[derive(Serialize, Deserialize, Clone, PartialEq, Debug)]
pub enum TargetTeamCondition {
	IsAllied,
	IsEnemy,
}
#[derive(Serialize, Deserialize, Clone, PartialEq, Debug)]
pub enum GameCondition {
	And(Vec<GameCondition>),
	Or(Vec<GameCondition>),
	Not(Box<GameCondition>),
	IsAtLeastTurn(i64),
	IsPhase(game::Phase),
	PositionIsUnoccupied(game::Position),
	PieceIsInPosition(game::Position, Box<TargetPieceCondition>),
	IsEndOfPhase,
}
impl GameCondition {
	pub fn check(&self, _source: &Rc<RefCell<Ability>>, targets: &VecDeque<Target>, game: &game::Game) -> ConditionResult {
		let mut targets = targets.clone();
		match targets.pop_front() {
			Some(s) => match s {
				Target::Game => match &self {
					GameCondition::IsAtLeastTurn(n) => if n <= game.get_turn() {
						ConditionResult::Ok
					}
					else {
						ConditionResult::InvalidTarget
					},
					GameCondition::IsPhase(p) => if p == game.get_phase() {
						ConditionResult::Ok
					}
					else {
						ConditionResult::InvalidTarget
					},
					_ => unimplemented!(),
				},
				_ => ConditionResult::InvalidTargetType,
			},
			None => ConditionResult::InsufficientTargets,
		}
	}
}

#[derive(Serialize, Deserialize, Clone, PartialEq, Debug)]
pub struct Trigger {
	state: bool,
	condition: Condition,
}
impl Trigger {
	pub fn new(state: bool, condition: Condition) -> Trigger {
		Trigger{state,condition}
	}
}

#[derive(Serialize, Deserialize, Clone, PartialEq, Debug)]
pub enum Condition {
	None,
	And(Vec<Condition>),
	Or(Vec<Condition>),
	Not(Box<Condition>),
	Piece(TargetPieceCondition),
	Ability(TargetAbilityCondition),
	Player(TargetPlayerCondition),
	Position(TargetPositionCondition),
	Resource(TargetResourceCondition),
	Game(GameCondition),
}
impl Condition {
	pub fn check(&self, _source: &Rc<RefCell<Ability>>, _targets: &VecDeque<Target>, _game: &game::Game) -> ConditionResult {
		match &self {
			Condition::None => ConditionResult::Ok,
			_ => unimplemented!(),
		}
	}
}
#[derive(Serialize, Deserialize, Clone, PartialEq, Debug)]
pub enum TargetCondition {
	None,
	Piece(TargetPieceCondition),
	Ability(TargetAbilityCondition),
	Player(TargetPlayerCondition),
	Position(TargetPositionCondition),
	Resource(TargetResourceCondition),
}

#[derive(Serialize, Deserialize, Clone, PartialEq, Debug)]
pub enum Effect {
	DoNothing,
	Optional(Box<Effect>),
	And(Vec<Effect>),
//	Or(Vec<Effect>),
	ChooseExactly(i32, Vec<Effect>),
	ChooseUpTo(i32, Vec<Effect>),
	ChooseAtLeast(i32, Vec<Effect>),
	Sequence(Vec<Effect>),
	If(Condition, Box<Effect>),
	AddAbility(Box<Ability>),
	ChangeResource(TargetResourceCondition, i32),
	SetResource(TargetResourceCondition, i32),
	MoveSelf(TargetPositionCondition),
	Destroy(Condition),
}
impl Effect {
	pub fn check(&self, source: &Rc<RefCell<Box<Ability>>>, targets: &mut VecDeque<Target>, activate: bool, game: &game::Game) -> ConditionResult {
		match &self {
			Effect::DoNothing => ConditionResult::Ok,
			Effect::MoveSelf(tcond) => match tcond.check(source, targets, game) {
				ConditionResult::Ok => match targets.pop_front() {
					Some(pos) => match pos {
						Target::Position(position) => {
							if activate {
								source.borrow().get_owner()
									.borrow_mut().reposition(position).expect("Move failed");
							}
							return ConditionResult::Ok;
						}, _ => ConditionResult::InvalidTargetType,
					}, None => ConditionResult::InsufficientTargets,
				},
				other => other
			},
			_ => unimplemented!(),
/*			Effect::ChangeResource(cond, amt) => {
				match target_iter.next() {
					Some(t) => match t {
						Target::Piece(p) => {
							match p.get_mut_resource(name) {
								Some(i) => {
									if !check_only {
										*i -= resource.clone();
									}
									ConditionResult::Ok
								},
								None => ConditionResult::InvalidTarget(100),
							}
						},
						_ => ConditionResult::InvalidTargetType(100),
					},
					None => Err(ActivationErr::InsufficientTargets)
				}
			},
			_ => Err(ActivationErr::TODO_getridofthis),
*/		} // match*/
	} // fn
} // impl

pub enum ConditionResult {
	Ok,
	MoreOptionalTargets,
//	NoOwner,
	InvalidTarget,
	InvalidTargetType,
	InsufficientTargets,
//	TooManyTargets,
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct Resource {
	amount: i32,
	default: i32,
	reset: Option<Trigger>,
}
impl Resource {
	pub fn new() -> Self {
		Resource{amount: 0, default: 0, reset: None}
	}
	pub fn set_amount(&mut self, amount: i32) -> &mut Self { self.amount = amount; self }
	pub fn set_default(&mut self, default: i32) -> &mut Self { self.default = default; self }
	pub fn set_reset(&mut self, reset: Option<Trigger>) -> &mut Self { self.reset = reset; self }

}
//TODO: implement trigger overwriting
impl std::ops::Add for Resource {
	type Output = Self;

	fn add(self, other: Self) -> Self {
		Self {
			amount: self.amount + other.amount,
			default: self.default + other.default,
			reset: self.reset.clone(),
		}
	}
}
impl std::ops::Sub for Resource {
	type Output = Self;

	fn sub(self, other: Self) -> Self {
		Self {
			amount: self.amount - other.amount,
			default: self.default - other.default,
			reset: self.reset.clone(),
		}
	}
}
impl std::ops::AddAssign for Resource {
	fn add_assign(&mut self, other: Self) {
		*self = Self {
			amount: self.amount + other.amount,
			default: self.default + other.default,
			reset: self.reset.clone(),
		};
	}
}
impl std::ops::SubAssign for Resource {
	fn sub_assign(&mut self, other: Self) {
		*self = Self {
			amount: self.amount - other.amount,
			default: self.default - other.default,
			reset: self.reset.clone(),
		};
	}
}
impl std::cmp::PartialEq for Resource {
	fn eq(&self, other: &Self) -> bool {
		self.amount == other.amount
	}
}

